var tag = document.createElement('p');
var text = document.createTextNode('Tutorix is the best e-learning platform');
tag.appendChild(text);
var element = document.getElementById('new');
element.appendChild(tag);

var text1 = document.getElementById('p1');

text1.addEventListener('click', () => {
  text1.innerHTML = 'New text!';
});

text1.addEventListener('mouseenter', () => {
  text1.style.color = 'gold';
});

var text2 = document.getElementById('p2');

text2.addEventListener('mouseenter', () => {
  text2.style.color = 'blue';
});

text2.addEventListener('mouseleave', () => {
  text2.style.color = 'black';
});
